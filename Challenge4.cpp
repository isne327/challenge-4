#include<iostream>
#include<vector>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<math.h>
using namespace std;
void num_ele(vector<int> vec,int);
void sum(vector<int> vec,int);
void high(vector<int> vec,int);
void low(vector<int> vec,int);
void mean(vector<int> vec,int);
void median(vector<int> vec,int);
void mode(vector<int> vec);
void sd(vector<int> vec,int);
void odd(vector<int> vec,int);
void even(vector<int> vec,int);
void sort(vector<int> vec,int);

int main()
{
	int size;
	srand(time(0));
	size = rand() %98+51; //random size of vector
	vector<int> vec(size);
	for(int i = 0 ; i<size ; i++)
	{
		srand(time(0)*i*i);
		vec[i] = rand()%100; //generate value from 0-100
	}
	num_ele(vec,size); //number of elements O(n)
	sum(vec,size); // sum of every elements O(n)
	high(vec,size); // highest value O(n)
	low(vec,size); // lowest value O(n)
	mean(vec,size); // mean of the elements O(n)
	median(vec,size); //median value O(n)
	mode(vec); //mode of the vector O(n)
	sd(vec,size); // SD of the vector O(n)
	even(vec,size); // showing every even number from vector O(n)
	odd(vec,size); // showing every odd number from vector O(n)
	sort(vec,size); // showing value from lowest to the highest O(n)
	
}

void num_ele(vector<int> vec,int size) // finding number of elements
{
	cout << "The number of elements = " << size <<endl;
}

void sum(vector<int> vec,int size) // Calculate the sum of elements
{
int sum;
for(int i=0 ; i<size ; i++)
	{
		sum=sum+vec[i];	
	}	
	cout << "The sum of all element = " << sum << endl;
}

void high(vector<int> vec,int size) //Finding the highest value
{
int max=0;
	for(int i=0 ; i<size ; i++ )
	{
		if(max < vec[i])
		{
		max = vec[i];	
		}	
	}
cout << "The highest value is " << max <<endl;
}

void low(vector<int> vec,int size) //Finding the lowest value
{
int min=101;
	for(int i=0 ; i<size ; i++ )
	{
		if(min > vec[i])
		{
		min = vec[i];	
		}	
	}
cout << "The lowest value is " << min <<endl;
	
}

void mean(vector<int> vec,int size) //Finding the average of the vector
{
	int sum,avr;
	for(int i=0 ; i<size ; i++)
	{
		sum = sum+vec[i];
	}
	avr=sum/size;
	cout << "The mean is " <<avr<<endl;
}

void median(vector<int> vec,int size) //Finding median of the vector
{
int k;	
	sort(vec.begin(),vec.end());
	k=(size+1)/2;
	cout << "Median is " << vec[k] << endl;		
}

void mode(vector<int> vec) //Finding mode from vector
{
vector<int> histogram(101,0);
	for(int i=0; i<100; i++)
	{
		++histogram[vec[i]];	
	}
	cout << "Mode is ";
	cout <<max_element(histogram.begin(),histogram.end()) - histogram.begin();
	cout << endl;
}

void sd(vector<int> vec,int size) //Finding SD of vector
{
	int sum,avr,hold1,hold2,hold3;
	for(int i=0 ; i<size ; i++)
	{
		sum = sum+vec[i];
	}
	avr=sum/size;
	for(int j=0 ; j<size ; j++)
	{
		hold1 = hold1 + pow(vec[j]-avr,2);
	}
	hold2 = hold1/size-1;
	hold3 = sqrt(hold2);
	cout << "Standard deviation is " << hold3 << endl;	
}

void even(vector<int> vec ,int size) //Showing all even number elements 
{
	int k;
	sort(vec.begin(),vec.end());
	cout << "Even number is ";
	for(int i=0 ; i<size ; i++)
	{
		if(vec[i]%2==0)
		{
			cout << vec[i] << " ";			
		}	
	}
	cout << endl<<endl;;
}

void odd(vector<int> vec,int size) //Showing all odd number elements
{
	int k;
	sort(vec.begin(),vec.end());
	cout << "Odd value is ";
	for(int i=0 ; i<size ; i++)
	{
		if(vec[i]%2!=0)
		{
			cout << vec[i] << " ";			
		}	
	}
	cout << endl <<endl;;	
}

void sort(vector<int> vec,int size)
{
sort(vec.begin(),vec.end());
cout << "Sorting value from lowest to highest" << endl;
cout << "**********************************************"<<endl;
	for(int q=0 ; q<size ;q++)
	{
		cout << vec[q] << endl;
	}
}


